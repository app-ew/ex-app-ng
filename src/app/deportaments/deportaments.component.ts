import { Component, OnInit } from '@angular/core';
import { Deportament } from '../deportament';
import { DeportamentService } from '../deportament.service';

@Component({
  selector: 'app-deportaments',
  templateUrl: './deportaments.component.html',
  styleUrls: ['./deportaments.component.scss'],
  providers: [DeportamentService]
})

export class DeportamentsComponent implements OnInit {
  deportaments: Deportament[] = [];

  constructor(private deportamentService: DeportamentService) { }

  ngOnInit() {
    this.deportamentService.getItems().subscribe((r: {
      result: Number
      items: Deportament[]
    }) => {
      if (r.result)
        this.deportaments = r.items
    })
  }

}
