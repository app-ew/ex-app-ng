import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeportamentsComponent } from './deportaments.component';

describe('DeportamentsComponent', () => {
  let component: DeportamentsComponent;
  let fixture: ComponentFixture<DeportamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeportamentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeportamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
