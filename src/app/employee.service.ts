import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from "./endpoints";

@Injectable()
export class EmployeeService {
    constructor(private http: HttpClient) { }

    getItems() {
        return this.http.get(Employee.all)
    }

    getItem(id) {
        return this.http.get(Employee.find(id))
    }

    update(item) {
        const body = {id: item.id, firstName: item.firstName, lastName: item.lastName, departmentId: item.departmentId};
        return this.http.post(Employee.update, body)
    }

    remove(id) {
        return this.http.delete(Employee.delete(id))
    }

    create(item) {
        return this.http.put(Employee.create, item)
    }

}
