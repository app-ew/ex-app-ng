import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeportamentComponent } from './deportament.component';

describe('DeportamentComponent', () => {
  let component: DeportamentComponent;
  let fixture: ComponentFixture<DeportamentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeportamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeportamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
