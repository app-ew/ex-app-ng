import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Deportament } from '../deportament';
import { DeportamentService } from '../deportament.service';

@Component({
  selector: 'app-deportament',
  templateUrl: './deportament.component.html',
  styleUrls: ['./deportament.component.scss'],
  providers: [DeportamentService]
})

export class DeportamentComponent implements OnInit {
  item: Deportament = {
    id: null,
    name: '',
    description: ''
  };

  constructor(
    private route: ActivatedRoute,
    private deportamentService: DeportamentService,
    private location: Location
  ) {}
  

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deportamentService.getItem(id)
      .subscribe((r: {result: Number, item: Deportament}) => this.item = r.item);
  }

  back() {
    this.location.back();
  }

}
