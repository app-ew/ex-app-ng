export class Employee {
  id: number;
  firstName: string;
  lastName: string;
  departmentId: number;
}

export class EmployeeAndDep  extends Employee {
  department: string = ''
}
