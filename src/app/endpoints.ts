let BaseUrl = 'http://localhost:3000/api'

export let Departaments = {
    all: `${BaseUrl}/Department`,
    find: (id = Number) => `${BaseUrl}/Department/${id}`,
    update: `${BaseUrl}/Department`,
    create: `${BaseUrl}/Department`,
    delete:  (id) => `${BaseUrl}/Department/${id}`,
}

export let Employee = {
    all: `${BaseUrl}/Employee`,
    find: (id = Number) => `${BaseUrl}/Employee/${id}`,
    update: `${BaseUrl}/Employee`,
    create: `${BaseUrl}/Employee`,
    delete:  (id) => `${BaseUrl}/Employee/${id}`,
}
