import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeportamentEditComponent } from './deportament-edit.component';

describe('DeportamentEditComponent', () => {
  let component: DeportamentEditComponent;
  let fixture: ComponentFixture<DeportamentEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeportamentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeportamentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
