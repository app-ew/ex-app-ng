import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Deportament } from '../deportament';
import { DeportamentService } from '../deportament.service';
import { Router } from "@angular/router"

@Component({
  selector: 'app-deportament',
  templateUrl: './deportament-edit.component.html',
  styleUrls: ['./deportament-edit.component.scss'],
  providers: [DeportamentService]
})

export class DeportamentEditComponent implements OnInit {
  item: Deportament = {
    id: null,
    name: '',
    description: ''
  };

  constructor(
    private route: ActivatedRoute,
    private deportamentService: DeportamentService,
    private location: Location,
    private router: Router
  ) { }


  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deportamentService.getItem(id)
      .subscribe((r: { result: Number, item: Deportament }) => {
        this.item = r.item

        if (this.route.snapshot.url[0].path == "DeportamentRemoveComponent") {
          let r = window.confirm("Точно удалить?")
          if (r == true) {
            this.deportamentService.remove(this.item.id).subscribe(r => {
              this.router.navigate(['/DeportamentsComponent'])
            });
          }
        }
      });
  }

  back() {
    this.location.back();
  }

  update() {
    this.deportamentService.update(this.item)
    this.router.navigate(['/DeportamentsComponent'])
  }


}
