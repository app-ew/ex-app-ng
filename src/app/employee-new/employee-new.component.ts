import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Employee } from '../employee';
import { Deportament } from '../deportament';
import { EmployeeService } from '../employee.service';
import { DeportamentService } from '../deportament.service';
import { Router } from "@angular/router"

@Component({
  selector: 'app-employee-new',
  templateUrl: './employee-new.component.html',
  styleUrls: ['./employee-new.component.scss'],
  providers: [EmployeeService, DeportamentService]
})

export class EmployeeNewComponent implements OnInit {
  deportaments: Deportament[] = []
  item: Employee = {
    id: null,
    firstName: '',
    lastName: '',
    departmentId: null
  };

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private deportamentService: DeportamentService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deportamentService.getItems()
      .subscribe((r: { result: Number, items: Deportament[] }) => this.deportaments = r.items);
  }

  update() {
    this.employeeService.create(this.item).subscribe(r => {
      this.router.navigate(['/EmployeeListComponent'])
    });
  }

}
