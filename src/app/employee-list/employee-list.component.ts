import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Employee,  EmployeeAndDep } from '../employee';
import { EmployeeService } from '../employee.service';
import { Deportament } from '../deportament';
import { DeportamentService } from '../deportament.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  providers: [EmployeeService, DeportamentService]
})

export class EmployeeListComponent implements OnInit {
  deportaments: Deportament[] = []
  items: Employee[] = [];
  items2: EmployeeAndDep[] = [];

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private deportamentService: DeportamentService,
    private location: Location
  ) { }


  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employeeService.getItems()
      .subscribe((r: { result: Number, items: Employee[] }) => {
        if (r.result) {
          this.items = r.items
          this.updateItems2()
        }
      });

    this.deportamentService.getItems()
      .subscribe((r: { result: Number, items: Deportament[] }) => {
        if (r.result) {
          this.deportaments = r.items
          this.updateItems2()
        }
      });
  }

  updateItems2() {
    if (this.items.length == 0) return
    if (this.deportaments.length == 0) return
    this.items2.length = 0
    for (let item of this.items) {
      this.items2.push({ ...item, department: this.deportaments.find(d => d.id == item.departmentId).name })
    }
  }

  back() {
    this.location.back();
  }

}
