import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Employee } from '../employee';
import { Deportament } from '../deportament';
import { EmployeeService } from '../employee.service';
import { DeportamentService } from '../deportament.service';

@Component({
  selector: 'app-employe',
  templateUrl: './employe.component.html',
  styleUrls: ['./employe.component.scss'],
  providers: [EmployeeService, DeportamentService]
})

export class EmployeComponent implements OnInit {
  deportaments: Deportament[] = []
  item: Employee = {
    id: null,
    firstName: '',
    lastName: '',
    departmentId: null
  };

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private deportamentService: DeportamentService,
    private location: Location
  ) { }


  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deportamentService.getItems()
      .subscribe((r: { result: Number, items: Deportament[] }) => this.deportaments = r.items);
    this.employeeService.getItem(id)
      .subscribe((r: { result: Number, item: Employee }) => this.item = r.item);
  }

  back() {
    this.location.back();
  }

}
