import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Employee } from '../employee';
import { Deportament } from '../deportament';
import { EmployeeService } from '../employee.service';
import { DeportamentService } from '../deportament.service';
import { Router } from "@angular/router"

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss'],
  providers: [EmployeeService, DeportamentService]
})

export class EmployeeEditComponent implements OnInit {
  deportaments: Deportament[] = []
  item: Employee = {
    id: null,
    firstName: '',
    lastName: '',
    departmentId: null
  };

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private deportamentService: DeportamentService,
    private location: Location,
    private router: Router
  ) { }


  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deportamentService.getItems()
      .subscribe((r: { result: Number, items: Deportament[] }) => this.deportaments = r.items);
    this.employeeService.getItem(id)
      .subscribe((r: { result: Number, item: Employee }) => {
        this.item = r.item

        if (this.route.snapshot.url[0].path == "EmployeeRemoveComponent") {
          let r = window.confirm("Точно удалить?")
          if (r == true) {
            this.employeeService.remove(this.item.id).subscribe(r => {
              this.router.navigate(['/EmployeeListComponent'])
            });
          }
        }

      });
  }

  back() {
    this.location.back();
  }

  update() {
    this.employeeService.update(this.item).subscribe(r => {
      this.router.navigate(['/EmployeeListComponent'])
    });
  }

}
