import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Departaments } from "./endpoints";

@Injectable()
export class DeportamentService {
    constructor(private http: HttpClient) { }

    getItems() {
        return this.http.get(Departaments.all)
    }

    getItem(id) {
        return this.http.get(Departaments.find(id))
    }

    update(item) {
        const body = {id: item.id, name: item.name, description: item.description};
        return this.http.post(Departaments.update, body).subscribe();
    }

    remove(id) {
        return this.http.delete(Departaments.delete(id))
    }

    create(item) {
        return this.http.put(Departaments.create, item)
    }

}
