import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DeportamentsComponent } from './deportaments/deportaments.component';
import { DeportamentComponent } from './deportament/deportament.component';
import { DeportamentEditComponent } from './deportament-edit/deportament-edit.component'
import { DeportamentNewComponent } from './deportament-new/deportament-new.component'

import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeComponent } from './employe/employe.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeNewComponent } from './employee-new/employee-new.component';


const routes: Routes = [
  { path: '', component: DeportamentsComponent },
  { path: 'DeportamentsComponent', component: DeportamentsComponent },
  { path: 'DeportamentNewComponent', component: DeportamentNewComponent },
  { path: 'DeportamentComponent/:id', component: DeportamentComponent },
  { path: 'DeportamentEditComponent/:id', component: DeportamentEditComponent },
  { path: 'DeportamentRemoveComponent/:id', component: DeportamentEditComponent },

  { path: 'EmployeeListComponent', component: EmployeeListComponent },
  { path: 'EmployeeComponent/:id', component: EmployeComponent },
  { path: 'EmployeeEditComponent/:id', component: EmployeeEditComponent },
  { path: 'EmployeeNewComponent', component: EmployeeNewComponent },
  { path: 'EmployeeRemoveComponent/:id', component: EmployeeEditComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }