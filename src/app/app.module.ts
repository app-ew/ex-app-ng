import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DeportamentsComponent } from './deportaments/deportaments.component';
import { HomeComponent } from './home/home.component';
import { DeportamentComponent } from './deportament/deportament.component';
import { DeportamentEditComponent } from './deportament-edit/deportament-edit.component';
import { DeportamentNewComponent } from './deportament-new/deportament-new.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeComponent } from './employe/employe.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeNewComponent } from './employee-new/employee-new.component';

@NgModule({
  declarations: [
    AppComponent,
    DeportamentsComponent,
    HomeComponent,
    DeportamentComponent,
    DeportamentEditComponent,
    DeportamentNewComponent,
    EmployeeListComponent,
    EmployeComponent,
    EmployeeEditComponent,
    EmployeeNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
