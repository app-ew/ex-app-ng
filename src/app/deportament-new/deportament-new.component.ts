import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Deportament } from '../deportament';
import { DeportamentService } from '../deportament.service';
import { Router } from "@angular/router"

@Component({
  selector: 'app-deportament',
  templateUrl: './deportament-new.component.html',
  styleUrls: ['./deportament-new.component.scss'],
  providers: [DeportamentService]
})

export class DeportamentNewComponent implements OnInit {
  item: Deportament = {
    id: null,
    name: '',
    description: ''
  };

  constructor(
    private route: ActivatedRoute,
    private deportamentService: DeportamentService,
    private router: Router
  ) { }


  ngOnInit(): void {
  }

  create() {
    this.deportamentService.create(this.item).subscribe(r => {
      this.router.navigate(['/DeportamentsComponent'])
    });
  }


}
