import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeportamentNewComponent } from './deportament-new.component';

describe('DeportamentNewComponent', () => {
  let component: DeportamentNewComponent;
  let fixture: ComponentFixture<DeportamentNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeportamentNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeportamentNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
